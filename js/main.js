var weatherConditions = new XMLHttpRequest();
var weatherForecast = new XMLHttpRequest();
var googleCurrentLocation = new XMLHttpRequest();
var cObj;
var fObj;

function load(){
    
    var city = document.getElementById("city").value;

    var url = 'http://api.wunderground.com/api/f029e46fd0232d12/geolookup/conditions/forecast/q/Australia/' + city + '.json';
    // GET THE FORECARST

    weatherForecast.open('GET', url, true);
    weatherForecast.responseType = 'text';
	weatherForecast.send();

	weatherForecast.onload = function() {
		if (weatherForecast.status === 200){
			fObj = JSON.parse(weatherForecast.responseText);
	    	console.log(fObj);

	    	document.getElementById("location").innerHTML = fObj.current_observation.display_location.full;
			document.getElementById("lat").innerHTML = fObj.current_observation.display_location.latitude;
			document.getElementById("lng").innerHTML = fObj.current_observation.display_location.longitude;
			document.getElementById("weather").innerHTML = fObj.current_observation.weather;
			document.getElementById("temperature").innerHTML = fObj.current_observation.temp_c;
			//document.getElementById("desc").innerHTML = fObj.current_observation.display_location.;

			document.getElementById("r1c1").innerHTML = fObj.forecast.simpleforecast.forecastday["0"].date.weekday;
			document.getElementById("r1c2").src = fObj.forecast.simpleforecast.forecastday["0"].icon_url;
			document.getElementById("r1c3").innerHTML = fObj.forecast.simpleforecast.forecastday["0"].high.celsius;
			document.getElementById("r1c4").innerHTML = fObj.forecast.simpleforecast.forecastday["0"].low.celsius;

			document.getElementById("r2c1").innerHTML = fObj.forecast.simpleforecast.forecastday["1"].date.weekday;
			document.getElementById("r2c2").src = fObj.forecast.simpleforecast.forecastday["1"].icon_url;
			document.getElementById("r2c3").innerHTML = fObj.forecast.simpleforecast.forecastday["1"].high.celsius;
			document.getElementById("r2c4").innerHTML = fObj.forecast.simpleforecast.forecastday["1"].low.celsius;

			document.getElementById("r3c1").innerHTML = fObj.forecast.simpleforecast.forecastday["2"].date.weekday;
			document.getElementById("r3c2").src = fObj.forecast.simpleforecast.forecastday["2"].icon_url;
			document.getElementById("r3c3").innerHTML = fObj.forecast.simpleforecast.forecastday["2"].high.celsius;
			document.getElementById("r3c4").innerHTML = fObj.forecast.simpleforecast.forecastday["2"].low.celsius;

			document.getElementById("r4c1").innerHTML = fObj.forecast.simpleforecast.forecastday["3"].date.weekday;
			document.getElementById("r4c2").src = fObj.forecast.simpleforecast.forecastday["3"].icon_url;
			document.getElementById("r4c3").innerHTML = fObj.forecast.simpleforecast.forecastday["3"].high.celsius;
			document.getElementById("r4c4").innerHTML = fObj.forecast.simpleforecast.forecastday["3"].low.celsius;

			// document.getElementById("r5c1").innerHTML = fObj.forecast.simpleforecast.forecastday["4"].date.weekday;
			// document.getElementById("r5c2").src = fObj.forecast.simpleforecast.forecastday["4"].icon_url;
			// document.getElementById("r5c3").innerHTML = fObj.forecast.simpleforecast.forecastday["4"].high.celsius;
			// document.getElementById("r5c4").innerHTML = fObj.forecast.simpleforecast.forecastday["4"].low.celsius;

		} //end if
	}; //end function
	


}
// GET THE CONDITIONS
load();








//http://api.wunderground.com/api/f029e46fd0232d12/geolookup/conditions/forecast/q/Australia/Sydney.json

// GET THE FORECARST
/*
weatherForecast.open('', '', true);
weatherForecast.responseType = 'text';
weatherForecast.send();

weatherForecast.onload = function() {
if (weatherForecast.status === 200){
	fObj = JSON.parse(weatherForecast.responseText);
	console.log(fObj);

} //end if
}; //end function
*/
